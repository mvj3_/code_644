#ActionView::Helpers::SanitizeHelper中有sanitize方法，可以在view 中使用
#如果想在model中使用，可以include ActionView::Helpers::SanitizeHelper. 但是会覆盖掉#ActiveRecord::Base的sanitize方法

after_save :sanitize_html

private
  def sanitize_html
    sanitizer = HTML::WhiteListSanitizer.new
    self.body = sanitizer.sanitize(self.body)
  end